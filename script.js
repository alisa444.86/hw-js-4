/*Метод объекта - это функция, которая хранится как его свойство и ползволяет выполнять какие-либо действия
с объектом или его лексическим окружением */


function CreateNewUser () {
    const firstName = prompt('What is your name?', '');
    const lastName = prompt('What is your lastname?','');

    const newUser = {
        _firstName: firstName,
        _lastName: lastName,

        get firstName() {
            return this._firstName;
        },

        set firstName(firstName) {
            this._firstName = firstName;
        },

        get lastName() {
            return this._lastName;
        },

        set lastName(lastName) {
            this._lastName = lastName;
        }
    };

    newUser.getLogin = () => {
        let login = `${newUser.firstName[0]}${newUser.lastName}`;
        login = login.toLowerCase();
        return login
    };

    /* Это реализация по ТЗ, но она не защищает свойство от изменения напрямую, как обычный геттер.

    newUser.setFirstName = function(firstName) {
        newUser.firstName = firstName;
    };

    newUser.setLastName = function(lastName) {
        newUser.lastName = lastName;
    };*/



    return newUser;
}

let user1 = CreateNewUser();
console.log(user1.getLogin());

